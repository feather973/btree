#include <stddef.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#include "btree.h"

/* INSERT ALGORITHM */
static int search(struct node *root, int data);
static void shift_and_insert(struct node *root, int idx, struct entry *up);
static void split(struct node *root, int data, int idx, struct entry *up);
static int __insert(struct node *root, int data, struct entry *up);

/* DELETE ALGORITHM */
static void borrow_left(struct node *root, int idx, struct node *left, struct node *right);
static void borrow_right(struct node *root, int idx, struct node *left, struct node *right);
static void combine(struct node *root, int idx, struct node *left, struct node *right);
static int reflow(struct node *root, int idx);

static int delete_mid(struct node *root, int idx, struct node *left);
static int delete_and_shift(struct node *root, int idx);
static int __delete(struct node *root, int data, int *success);

// data 보다 큰 idx 리턴, 없으면 0
static int search(struct node *root, int data)
{
	int idx = MAX_ENTRY_IDX;		// 맨 끝부터 비교

	if (data < root->entries[0].data) {
		idx = 0;
	} else {
		while (data < root->entries[idx].data) {
			idx--;
		}
	}

	return idx;
}

// 노드 내 엔트리를 우측으로 밀고, 새 엔트리를 넣는다.
static void shift_and_insert(struct node *root, int idx, struct entry *up)
{
	int cursor = MAX_ENTRY_IDX; 
	int data = up->data;
	struct node *right = up->right;
	
	while (cursor != idx) {
		root->entries[cursor] = root->entries[cursor-1];
		cursor--;
	}

	root->entries[cursor].data = data;
	root->entries[cursor].right = right;	
}

// 노드는 최소 entry 만 갖고, 나머지는 신규 right 노드에게 넘겨준다.
// data : 원래 넣을려고 한 값 (비교용)
// idx : 노드에 data 를 넣을 위치
// up : 하위레벨에서 넣어달라고 올린 값
static void split(struct node *root, int data, int idx, struct entry *up)
{
	int minimum = floor(ORDER/2);		// [ORDER/2]
	struct node *right;
	int from, to;

	int i;

	// 신규 right 노드
	right = calloc(sizeof(struct node), 1);
	if (right) {
		// 초기화
		for (i=0; i<MAX_ENTRY; i++)
			right->entries[i].data = DEFAULT_DATA;
	} else {
		printf("[%d] insert fail, cannot create new right node\n", __LINE__);
		return;
	}

	// 넣을 값이 [ORDER/2] 값 이상이면, [ORDER/2] 값은 오른쪽으로 이동되면 안된다.
	if (data >= root->entries[minimum].data)
		from = minimum + 1;
	else
		from = minimum;
	to = 0;

	while (from < root->count) {
		right->entries[to].data = root->entries[from].data;
		right->entries[to].right = root->entries[from].right;

		from++;
		to++;
	}

	root->count -= to;
	right->count += to;	

	// 넣을 값 대신 up 을 넣어준다
	if (idx < minimum) {		// 원래 노드 
		if (data < root->entries[idx].data)		
			shift_and_insert(root, idx, up);
		else
			shift_and_insert(root, idx+1, up);
		root->count++;
	} else {					// 신규 right 노드
		shift_and_insert(right, idx - minimum, up);
		right->count++;
	}

	// 새 up 엔트리 값은 root 의 [ORDER/2] 에서 꺼내온다.
	up->data = root->entries[minimum].data;
	root->count--;

	up->right = right;
	right->first = root->entries[minimum].right;	// up 엔트리의 부분트리 값 < right 값
}

// 단말노드까지 내려갔다, 올라오면서 데이터를 넣을 곳을 찾는다.
// 넣을 곳이 꽉찬노드면 split 을 호출하고, 상위 레벨로 [ORDER/2] 에 있는 값을 넘긴다.
static int __insert(struct node *root, int data, struct entry *up)
{
	int taller = 0;				// 상위 레벨로 값을 넘기는 지
	int idx;

	// 2. 단말 노드 발견
	if (!root) {
		up->data = data;		// 단말 노드 만나면 data 를 넣어준다.
		up->right = NULL;		

		taller = 1;				// 상위 레벨로 값을 넘긴다.
		return taller;
	}

	// 1. 현재 노드에서 엔트리를 넣을 곳을 찾는다. 찾았으면 단말노드를 만날때까지 내려간다.
	idx = search(root, data);		// data > root->entries[idx].data 인 idx 를 내림차순으로 검색
	if (!idx) {
		if (root->entries[idx].data < data)
			taller = __insert(root->entries[0].right, data, up);	// 0번째 엔트리의 오른쪽
		else
			taller = __insert(root->first, data, up);				// 0번째 엔트리의 왼쪽
	} else {
		taller = __insert(root->entries[idx].right, data, up);		// 넣을려고 한 엔트리의 right subtree
	}

	// 3. 하위 레벌에서 넘긴 값이 있으면 처리해준다.	
	if (taller) {
		if (root->count == MAX_ENTRY) {
			split(root, data, idx, up);

			taller = 1;				// split 이후엔 항상 상위 레벨로 값을 넘긴다.
		} else {
			if (root->entries[idx].data < up->data)	{
				shift_and_insert(root, idx+1, up);					// up 에 든 값이 더 큰 경우, 우측에 넣어줘야 한다.
			}
			else
				shift_and_insert(root, idx, up);					// 넣을려고 한 엔트리에 넣어준다.
			root->count++;
			up->data = DEFAULT_DATA;
			up->right = NULL;

			taller = 0;		
		}
	}

	return taller;
}

// 루트노드가 없을 경우 새로 생성, split 이 아직 처리안됬었다면 새로 root 생성해서 넣어준다
struct node *insert(struct node *root, int data)
{
	struct entry up = { DEFAULT_DATA, NULL };
	int taller;
	struct node *new_root;

	int i;

	// 처음 호출
	if (!root) {
		root = calloc(sizeof(struct node), 1);	

		if (root) {
			// 초기화
			for (i=0; i<MAX_ENTRY; i++)
				root->entries[i].data = DEFAULT_DATA;

			// data 저장
			root->entries[0].data = data;
			root->count = 1;
			
			return root;
		} else {
			printf("[%d] insert fail, cannot create new root node\n", __LINE__);
			return NULL;
		}
	}

	// 처음 호출이 아니면, split 처리 유무를 본다
	taller = __insert(root, data, &up);
	if (taller) {
		// 미처리 : 새 루트를 만들고, up 을 첫번쨰 엔트리로
		new_root = calloc(sizeof(struct node), 1);
		
		if (new_root) {
			// 초기화
			for (i=0; i<MAX_ENTRY; i++)
				new_root->entries[i].data = DEFAULT_DATA;

			// data 저장 및 root 연결
			new_root->entries[0].data = up.data;
			new_root->entries[0].right = up.right;	
			new_root->first = root;
			new_root->count = 1;

			return new_root;
		} else {
			printf("[%d] insert fail, cannot create new root node\n", __LINE__);
			return NULL;
		}
	}

	// split 처리된 경우는 별다른 조치없이 리턴
	return root;
}

// 트리 inorder 순회
void traverse(struct node *root)
{
	int idx = 0;
	int count;

	if (!root) {
		printf("traverse receive root NULL\n");
		return;
	}
	count = root->count;		// 현재 노드의 count 만큼 순회

	if (root->first)
		traverse(root->first);

	for (idx=0; idx<count; idx++) {
		printf("%d ", root->entries[idx].data);

		if (root->entries[idx].right)
			traverse(root->entries[idx].right);
	}
}

// root 의 오른쪽 부분트리는 부족상태
static void borrow_left(struct node *root, int idx, struct node *left, struct node *right)
{
	int rightmost = right->count - 1;
	int cursor = right->count -1;

	int left_rightmost = left->count - 1;

	while (cursor > 0) {
		right->entries[cursor].data = right->entries[cursor-1].data;
		right->entries[cursor].right = right->entries[cursor-1].right;
		cursor--;
	}
	
	right->entries[0].data = root->entries[idx].data;	

	right->entries[0].right = right->first; 
	right->first = left->entries[left_rightmost].right;

	root->entries[idx].data = left->entries[left_rightmost].data;

	right->count++;
	left->count--;
}

// root 의 왼쪽부분트리는 부족상태
static void borrow_right(struct node *root, int idx, struct node *left, struct node *right)
{
	int rightmost = left->count - 1;
	int cursor = 1;

	left->entries[rightmost+1].data = root->entries[idx].data;
	left->entries[rightmost+1].right = right->first;				// !! of course, not right
	left->count++;

	root->entries[idx].data = right->entries[0].data;
	right->first = right->entries[0].right;

	while (cursor < right->count) {
		right->entries[cursor-1] = right->entries[cursor];
		cursor++;
	}
	right->count--;
}

// 부모(root) 와 자식을 left 로 넘긴다
static void combine(struct node *root, int idx, struct node *left, struct node *right)
{
	int from, to;

	// root.entries[idx] --> left
	to = left->count;
	left->entries[to].data = root->entries[idx].data;
	left->entries[to].right = right->first;

	root->count--;
	left->count++;

	// right --> left
	from = 0;
	to = left->count;

	while (from < right->count) {
		left->entries[to].data = right->entries[from].data;
		left->entries[to].right = right->entries[from].right;

		from++;
		to++;
	}

	left->count = left->count + from;
	//right->count = 0;
	free(right);		// free right

	// shift root entries
	from = idx+1;
	while (from < root->count) {
		root->entries[from-1] = root->entries[from];
		from++;
	}
}

#define __NO_UNDERFLOW	0
#define __UNDERFLOW		1

// 삭제 후 underflow 가 된 노드는 부모를 거쳐서 데이터를 빌려오거나, 합쳐야 한다.
static int reflow(struct node *root, int idx)
{
	struct node *left;
	struct node *right;
	int min = floor(ORDER/2);
	int underflow;

	if (!idx)
		left = root->first;
	else
		left = root->entries[idx-1].right;

	right = root->entries[idx].right;
	if (right->count > min) { 
		borrow_right(root, idx, left, right);		// 오른쪽에서 빌림
		underflow = __NO_UNDERFLOW;	
	} else if (left->count > min) {
		borrow_left(root, idx, left, right); 		// 왼쪽에서 빌림
		underflow = __NO_UNDERFLOW;
	} else {										// 못빌리므로, 합침
		combine(root, idx, left, right);	

		if (root->count < min)
			underflow = __UNDERFLOW;	
		else
			underflow = __NO_UNDERFLOW;
	}

	return underflow;
}

// left 의 rightmost 엔트리를 찾을떄까지 제귀
static int delete_mid(struct node *root, int idx, struct node *left)
{
	int underflow;
	int rightmost;
	struct node *right;

	if (!left->first) {			// left 는 단말노드
		rightmost = left->count - 1;
		root->entries[idx].data = left->entries[rightmost].data;		// left->entries[rightmost].right 는 보지않는다
		left->count--;

		underflow = left->count < floor(ORDER/2) ? __UNDERFLOW : __NO_UNDERFLOW;
	} else {					// 단말노드가 아님
		rightmost = left->count - 1;
		underflow = delete_mid(root, idx, left->entries[rightmost].right);

		if (underflow)
			underflow = reflow(left, rightmost);
	}
	return underflow;
}

static int delete_and_shift(struct node *root, int idx)
{
	int cursor = idx+1;	
	int minimum = floor(ORDER/2);

	while (cursor < root->count) {
		root->entries[cursor-1] = root->entries[cursor];
		cursor++;
	}

	root->count--;	
	
	if (root->count < minimum)
		return 1;
	else
		return 0;
}

static int __delete(struct node *root, int data, int *success)
{
	int idx;
	struct node *left;
	int underflow;

	struct node *subtree;

	if (!root)
		return 0;

	idx = search(root, data);	
	if (data == root->entries[idx].data) {
		*success = 1;
		
		if (!root->entries[idx].right) {
			underflow = delete_and_shift(root, idx);
		} else {
			if (idx > 0)
				left = root->entries[idx-1].right;
			else
				left = root->first;

			underflow = delete_mid(root, idx, left);

			if (underflow)
				underflow = reflow(root, idx);
		}
	} else {
		if (data < root->entries[0].data)
			subtree = root->first;
		else
			subtree = root->entries[idx].right;

		underflow = __delete(subtree, data, success);

		if (underflow)
			underflow = reflow(root, idx);			// 하위 노드에서 overflow 올라오면, 받아서 처리
	}

	return underflow;
}

struct node *delete(struct node *root, int data)
{
	struct node *del_node;
	int success;			

	if (!root) {
		printf("[%d] delete %d fail, root is null\n", __LINE__, data);
		return NULL;
	}

	__delete(root, data, &success);
	if (success) {
		if (!root->count) {	
			del_node = root;
			root = root->first;
			free(del_node);
		}
	}

	return root;
}

int main()
{
	struct node *root = NULL;

	int tc[16] = { 10, 20, 40, 50, 30, 1000, 2, 4030, 50301, 4012, 403104, 403140, 123124, 11, 42, 32 };
	int delete_tc[16] = { 2, 30, 50301, 123124, 403140, 32, 42, 1000, 403104, 10, 20, 50, 11, 40, 4030, 4012 }; 
	int i;

	for (i=0; i<16; i++) {
		printf("enter %d\n", tc[i]);
		root = insert(root, tc[i]);
		traverse(root);
		printf("\n");
	}

	for (i=0; i<16; i++) {
		printf("delete %d\n", delete_tc[i]);
		// root 변경은 delete 가 수행
		root = delete(root, delete_tc[i]);

		traverse(root);
		printf("\n");
	}

	return 0;
}
