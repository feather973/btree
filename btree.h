#define ORDER			(5)
#define MAX_ENTRY		(ORDER-1)
#define MAX_ENTRY_IDX	(MAX_ENTRY-1)

#define DEFAULT_DATA	0x7fffffff

struct entry {
	int data;				
	struct node *right;
};

struct node {
	int count;
	struct node *first;
	struct entry entries[MAX_ENTRY];
};

struct node *insert(struct node *root, int data);
struct node *delete(struct node *root, int data);
void traverse(struct node *root);
